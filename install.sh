# Update Packages
apt-get update -y

# Upgrade Packages
apt-get upgrade -y

# Basic Linux Stuff
apt-get install -y git
# Apache
apt-get install -y apache2

# Enable Apache Mods
a2enmod rewrite

# Add Onrej PPA Repo
apt-add-repository ppa:ondrej/php
apt-get update

# Set PHP version
PHPVER=8.1

# Install PHP
apt-get install -y php$PHPVER

# Install XDebug and allow code coverage reports
apt-get install -y php-xdebug
echo 'xdebug.mode=coverage' >> /etc/php/$PHPVER/cli/php.ini

# PHP Apache Mod
apt-get install -y libapache2-mod-php$PHPVER

# Restart Apache
service apache2 restart

# PHP Mods
apt-get install -y php$PHPVER-common
apt-get install -y php$PHPVER-mcrypt
apt-get install -y php$PHPVER-zip
sudo apt-get install -y php-mbstring
sudo apt-get install -y php-xml

sudo service apache2 reload

# curl stuff
sudo apt-get install php$PHPVER-curl
sudo service apache2 restart

# Set MySQL vars
DBHOST=localhost
DBNAME=intelli_tracker
DBUSER=intelli
DBPASSWD=8NrFdPKVN4tT5PEAnGsBQS5NX8pAaD

# set root PW
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"

# install mysql
apt-get install -y mysql-server mysql-client

# Allow External Connections on your MySQL Service --------- LOOK!!!!!
# run the following command and use the details to make a connection via SSH Tunnel: vagrant ssh-config

# Log into mysql Create the database and grant privileges
CMD="mysql -uroot -p$DBPASSWD -e"

$CMD "CREATE DATABASE $DBNAME;"
$CMD "CREATE USER '$DBUSER'@'%' IDENTIFIED BY '$DBPASSWD';"
$CMD "GRANT ALL PRIVILEGES ON $DBNAME.* TO '$DBUSER'@'%';"
$CMD "FLUSH PRIVILEGES;"
$CMD "quit"

# PHP-MYSQL lib
apt-get install -y php$PHPVER-mysql

# sqlite (for testing)
sudo apt-get install -y php-sqlite3

# Restart Apache
sudo service apache2 restart

# install composer
sudo apt update
sudo apt install php-cli unzip
cd ~
curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php
HASH=`curl -sS https://composer.github.io/installer.sig`
echo $HASH
php -r "if (hash_file('SHA384', '/tmp/composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer

# PERMISSIONS
sudo chmod 777 /var/www/html -R

# change site root (VM ONLY)
sudo cp /var/www/html/000-default.conf /etc/apache2/sites-available/000-default.conf
sudo systemctl restart apache2

# non VM ssh key stuffs (save it in it's standared place)
# ssh-keygen
# cat ~/.ssh/id_rsa.pub # copy the output and ADD to the OCSI github account

# composer, laravel stuff and delete VM index.html
cd /var/www/html
sudo rm index.html
# non VM: mkdir ocsi-insight-fe
# non VM: mkdir ocsi-insight-be
# non VM: cd ocsi-insight-be
# non VM: git clone git@github.com:dev-ocsi/ocsi-insight-be.git .
# non VM: probably will need to "git checkout develop"
# non VM: change site root sudo cp /var/www/html/ocsi-insight-be/000-default.conf /etc/apache2/sites-available/000-default.conf
# non VM: sudo nano /etc/apache2/sites-available/000-default.conf
# non VM: REMOVE "public" from the DocumentRoot AND Directory paths
# non VM: sudo systemctl restart apache2

# composer require laravel/sanctum
composer install
php artisan config:clear
php artisan key:generate
cp .env.example .env
php artisan migrate
php artisan db:seed

# non VM: cd /var/www/html/ocsi-insight-fe
# non VM: git clone git@github.com:dev-ocsi/ocsi-insight-fe.git .
# non VM: probably will need to "git checkout develop"
# non VM: cd /var/www/html/
# non VM: sudo nano .htaccess
# non VM: add to the .htaccess file the contents of "C:\Code\Git Repositories\ocsi-insight-be\htaccess file example" and ammend if necessary
# non VM: if required, update our DNS server with the new servers ip address (cloudfare)
# non VM: you may need to add peoples ssh keys to the server, go here and add their .pub key on a new line: sudo nano ~/.ssh/authorized_keys

# Java
sudo apt install default-jre -y

# Jenkins - See https://ocsi-jira.atlassian.net/wiki/spaces/DEV/pages/3385524243/Jenkins confluence page for more details
# sudo apt install default-jre
# wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
# sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
#     /etc/apt/sources.list.d/jenkins.list'
# sudo apt-get update
# sudo apt-get install jenkins
# non VM: In /etc/default/jenkins, change JENKINS_ARGS="" to JENKINS_ARGS="--prefix=/jenkins"

# Let's Encrypt SSL
# non VM: sudo snap install core; sudo snap refresh core
# non VM: sudo snap install --classic certbot
# non VM: sudo ln -s /snap/bin/certbot /usr/bin/certbot
# non VM: sudo certbot --apache

# non VM: Example for adding more to stg.communityinsight.org certificate..NOTE...for the staging server you have to add http and https access to the firewall without the office IP address (this is carried out in the digital ocean droplet / networking config)...don't forget to remove them when you're finished!
# certbot --expand -d stg.communityinsight.org,stg.communityinsight.org,stg.api.communityinsight.org,stg.local.communityinsight.org,stg.wales.communityinsight.org,stg.scotland.communityinsight.org,stg.client-one.communityinsight.org,stg.client-two.communityinsight.org,stg.client-three.communityinsight.org,stg.client-four.communityinsight.org
# ALSO go here: sudo nano /etc/apache2/sites-enabled/000-default-le-ssl.conf and under the "ServerName" entry add the following: (minus the hashes)
#ServerAlias stg.api.communityinsight.org
#ServerAlias stg.local.communityinsight.org
#ServerAlias stg.wales.communityinsight.org
#ServerAlias stg.scotland.communityinsight.org
#ServerAlias stg.client-one.communityinsight.org
#ServerAlias stg.client-two.communityinsight.org
#ServerAlias stg.client-three.communityinsight.org
#ServerAlias stg.client-four.communityinsight.org
# DON'T forget to restart apache "sudo systemctl restart apache2"

# non VM: add redirect to https on /etc/apache2/sites-enabled/000-default.conf, example:
# RewriteCond %{SERVER_NAME} =stg.communityinsight.org
# RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]

# non VM: Add to /etc/apache2/sites-enabled/000-default-le-ssl.conf (minus the hashes!):
# RewriteRule ^/jenkins(.*)$ http://localhost:8080/jenkins$1 [P,L]
# ProxyPassReverse /jenkins http://localhost:8080/jenkins
# ProxyRequests       Off
# 
# <Proxy http://localhost:8080*>
#   Order deny,allow
#   Allow from all
# </Proxy>

# non VM: sudo chmod 777 /var/log/apache2 -R
# non VM: error log: nano /var/log/apache2/error.log
# non VM: sudo tail -f /var/log/apache2/error.log
# non VM: sudo systemctl restart apache2

#sudo nano /etc/php/$PHPVER/apache2/php.ini
#memory_limit = 500M                                                                                                         
#max_execution_time = 240
#enable curl
#sudo systemctl restart apache2
