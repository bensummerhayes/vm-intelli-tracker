
<h2>PLEASE READ ALL OF THIS DOCUMENT BEFORE YOU START!</h2>

**There is an assumption you have Git, Vagrant and Virtual machine installed on your computer...if not then you need to install them and then come back here when you're ready!**

**PLEASE NOTE:** There are two repos being discussed here, 'intelli-tracker-be' - actual codebase and 'vm-intelli-tracker' - the VM

* NOTE: Make sure you have added your ssh key to **both** repos in Git Hub...any issues or questions then speak to Ben, make sure you have your ssh key at hand!
* NOTE: Insure the following folder exists: "C:/Code/Git Repositories/".

0. Open a bash and CD here: 'cd  /c/Users/YOUR_USERNAME'. Copy, paste and execute the following git command (**for the VM**): 'git clone git@github.com:dev-ocsi/vm-intelli-tracker.git' (say 'yes' to the RSA question).
1. Still in bash, CD here: 'cd  /c/Users/Git\ Repositories', now copy, paste and execute the following git command (**for the codebase**): 'git clone git@github.com:dev-ocsi/intelli-tracker-be.git' (say 'yes' to the RSA question).
2. In bash CD into the intelli-tracker-be folder, checkout and pull the develop branch like this: 'git checkout develop & git pull'.
3. Still in bash CD to the above mentioned VM, like this:"'cd  /c/Users/YOUR_USERNAME/vm-intelli-tracker'" and checkout the latest release.
4. Now run the 'vagrant up' command, sit back and wait...go and have a cup of tea...it will take ages to finish the composer stuff...ages until its all installed and running.
5. Run the 'vagrant ssh' command and hopefully you are now inside the new box.
6. Once inside the new box go here: cd /var/www/html, have a look at what is inside ('ls')...hopefully you will see the above mention intelli-tracker-be repo file structure, if not then there is something wrong :/
7. Now for your hosts file...open notepad as an Admin.
8. go here: "C:\Windows\System32\drivers\etc" (OR "sudo nano /etc/hosts" for linux) and open your hosts file. REMEMBER to change the file type to All Files if you can't see it in the folder.
9. Add the following to the bottom of the page:

				192.168.63.254 dev.api.intelli-tarcker.org

10. In your browser go to the above url (http://dev.api.communityinsight.org) and you should see a welcome page.
11. In this folder there is a desktop shortcut called "add me to your desktop! intelli-tracker VM" use this if you want a shortcut to spin the vm up! - You will have to edit it with your windows username
12. Job Done!

* NOTE: You can access the database using an external IDE. The port forwarding is done automatically.

